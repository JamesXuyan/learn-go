/**
  @author: Xu·Yan
  @date: 2021/4/23
  @description Go函数调用-> 多个返回值
**/
package main

import "fmt"

/**
 * @author 多个返回值
 * @date  2021/4/23 5:37 下午
 * @param
 * @return
 **/
func getSubAndSum(s1 int, s2 int) (int, int) {

	sum := s1 + s2
	sub := s1 - s2
	return sum, sub

}

//调用函数

func main() {

	a := 2
	b := 3
	sum, sub := getSubAndSum(a, b)

	fmt.Println("sum=", sum)
	fmt.Println("sub=", sub)

	//go 语言特性：如果一个变量没有被引用，将直接报错...
	//优点:直接排除垃圾回收

}

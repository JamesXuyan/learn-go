/**
  @author: Xu·Yan
  @date: 2021/4/23
  @description Go语言的转义字符(escape char)
**/
package main

import "fmt"

func main() {

	// 转义字符 \t：表示一个制表符，通常可以用来排版
	fmt.Println("tom\tjack")

	// \n: 换行符
	fmt.Println("hello\nworld")

	// \\: 一个/ 需要2个\\转义
	fmt.Println("C:\\User\\DownLoad\\Images")

	// \: 表示一个'
	fmt.Println("jack说\"I Love you\"")

	// \r: 回车，从当前行的最前面开始覆盖调以前的内容 -> 打印结果: 张飞厉害雪山飞狐
	fmt.Println("天龙八部雪山飞狐\r张飞厉害")

	//使用以上所学练习
	fmt.Println("姓名\t年龄\t籍贯\t住址\n小徐\t18\t中国\t北京")

	fmt.Print("哈哈->") //不换行
	fmt.Println("哈哈") //换行

	fmt.Println("\t*\t\t\t\t\t*\n     *\t\t*     I Love GoLang\t*\t   *\n\t*\t\t\t\t\t*\n\t  *" +
		"\t\t\t\t     *\n\t     *\t\t\t\t  *\n\t        *\t\t       *\n\t\t   *\t\t   *\n\t\t      *\t\t*\n\t\t\t *   *\n\t\t\t   *")

	/*GoLang程序编写规则
		1.go文件的后缀.go
		2.go程序区分大小写
		3.go的语句后，不需要带括号
		4.go定义的变量，或者import包，必须使用，如果没有使用会报错
		5.go中，不要把多条语言放在同一行，否则报错
		6.go中的大括号成对出现，风格如下
		func main(){
		//语句
	}

	*/

}

/**
  @author: Xu·Yan
  @date: 2021/4/26
  @description GoLang数据类型学习
**/
package main

import (
	"fmt"
	"unsafe"
)

func main() {

	var i = 1
	var j = 2
	var r = i + j // 根据数据类型做加法预算
	fmt.Println("r=", r)

	var s = "hello"
	var x = "world"
	var y = s + x //数据类型为字符串，做拼接操作
	fmt.Println("y=", y)

	/* 数据类型基本介绍
	整体分为两大类
	1: 基本数据类型
		1.1:数值型
			1.1.1:整数类型(int,int8,int16,int32,int64,uint,uint8,uint16,uint32,uint64,byte)
			1.1.2:浮点类型(float32,float64)
		1.2:字符型(没有专门的字符型,使用byte来保存单个字母字符)
		1.3:布尔型(bool)
		1.4:字符串(string)
	2: 派生/复杂数据类型
		2.1:指针(Pointer)
		2.2:数组
		2.3:结构体
		2.4:管道
		2.5:函数
		2.6:切片(slice)
		2.7:接口
		2.8:map
	*/

	var a = 100
	fmt.Printf("a的数据类型 %T \n", a)

	//如何在程序查看某个变量的字节大小和数据类型
	var b = 100
	fmt.Printf("b 的数据类型 %T b占用的字节数是 %d ", b, unsafe.Sizeof(b))
	fmt.Println("\n")

	//关于浮点数在机器中存放形式的简单说明，浮点数=符号位+指数位+尾数位
	var price = 89.12
	fmt.Println("price=", price)
	var num1 = -0.00089
	var num2 = -7809656.09
	fmt.Println("num1=", num1, "num2=", num2)

	//测试浮点数精度
	var num3 float32 = -123.0000901
	var num4 float64 = -123.0000901
	fmt.Println("num3=", num3, "num4=", num4) // num3= -123.00009 num4= -123.0000901 通过结果查看64位精度比32位更加准确

	//浮点型的存储分为三个部分: 符号位+指数位+尾数位 在存储过程中，精度会丢失

	var num5 = 1.1
	fmt.Printf("num5 的数据类型是 %T \n", num5)

	// 浮点数常量有两种表示形式
	// 1.十进制数形式: 如5.12  .123 (必须有小数点)
	num6 := 5.12
	num7 := .123
	fmt.Println("num6=", num6, "num7=", num7)

	//2. 科学计数法形式
	num8 := 5.1234e2
	num9 := 5.1234e2
	num10 := 5.1234e-2
	fmt.Println("num8=", num8)
	fmt.Println("num8=", num9)
	fmt.Println("num8=", num10)

}

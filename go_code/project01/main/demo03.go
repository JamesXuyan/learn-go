/**
  @author: Xu·Yan
  @date: 2021/4/25
  @description GoLang变量学习
**/
package main

import "fmt"

/**
  多个返回值
*/
func getVal(num1 int, num2 int) (int, int) {
	sum := num1 + num2
	sub := num2 - num1
	return sum, sub
}

func main() {

	sum, sub := getVal(30, 30)
	fmt.Println("sum=", sum, "sub=", sub)
	sum2, _ := getVal(10, 30) //只取第一个返回值 为什么下划线没有报错，严格意义上来说下划线不属于英文字母，不算变量
	fmt.Println("sum=", sum2)

	/* 变量的使用步骤
	1.声明变量(定义变量)
	2.非变量赋值
	3.使用变量
	*/
	//定义变量/声明变量
	var i int
	// 给 i 赋值
	i = 10
	//使用变量
	fmt.Println(i)
	//GoLang 变量的三种使用方式
	//1.指定变量类型，声明后若不赋值，将使用默认值
	var s int      //-> int 的默认值为0
	fmt.Println(s) //打印结果0
	//2.根据值自行判定变量类型(类型推导)
	var num = 10.11 // 将鼠标悬停变量会提示该变量类型为 float64
	fmt.Println("num=", num)
	//3.省略var
	name := "jack" // 系统会根据值来推导该变量属于什么类型 -> 鼠标悬停name 显示该变量类型为 String
	fmt.Println(name)
	//4.多变量声明
	var n1, n2, n3 int
	fmt.Println(n1, n2, n3)

	//一次性声明多个变量的方式二
	var s1, age, s2 = 100, "jack", 888 //横向赋值，s1对应的是100，age对应的是jack，s2对应的是888 ,中间 = 号隔开
	fmt.Println("s1=", s1, "age=", age, "s2=", s2)

	//一次性声明多个变量的方式二
	x1, sex, x3 := 100, "jack", 888 //使用类型推导的方式，根据对应的值判断变量类型,中间 := 隔开
	fmt.Println("x1=", x1, "sex=", sex, "x3=", x3)

	// -> 如何一次性声明多个全局变量[在Go中函数外部定义变量就是全部变量]
	var y1 = 100
	var y2 = 200
	var yName = "jack"
	// 上面的声明方式也可以改成一次性声明

	var (
		y3    = 300
		y4    = 400
		xName = "roes"
	)
	fmt.Println(y1, y2, yName)
	fmt.Println(y3, y4, xName)

	//5.该区域的数据值可以在同一类型范围内不断变化(重点)

	var k = 10
	k = 30
	k = 50
	fmt.Println("k=", k) // 打印结果: 50 按照程序执行顺序依次向下，每次执行都相当于重新赋值

	//6.变量在同一个作用域(在一个函数或者在代码块)内不能重名
	//上面已经声明了变量名 K 所以再次声明会报错
	/*	var k = 20
		k := 21
		fmt.Println(k)*/

	//7.变量 = 变量名 + 数据类型 + 值
	var m int = 10
	fmt.Println(m)

	//8.GoLang的变量如果没有赋值，编译器会使用默认值，比如int默认值 0   String默认值 -> " "  小数默认值0

}
